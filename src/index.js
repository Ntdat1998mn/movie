import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { compose, createStore } from "redux";
import { Provider } from "react-redux";
import { configureStore } from "@reduxjs/toolkit";
import userSlice from "./Redux-toolkit/slice/userSlice";
import formSlice from "./Redux-toolkit/slice/formSlice";
import formRegSlice from "./Redux-toolkit/slice/formRegSlice";
import purchaseRedux from "./Redux-toolkit/slice/purchaseRedux";
import fastTicketRedux from "./Redux-toolkit/slice/fastTicketRedux";
import movieSlice from "./Redux-toolkit/slice/movieSlice";

export const store_toolkit = configureStore({
  reducer: {
    userSlice: userSlice,
    formSlice: formSlice,
    formRegSlice: formRegSlice,
    purchaseRedux: purchaseRedux,
    fastTicketRedux: fastTicketRedux,
    movieSlice: movieSlice,
  },
});

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={store_toolkit}>
    <App />
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
