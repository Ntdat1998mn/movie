import React from "react";
import Header from "../Pages/HomePage/Header/Header";
import { Desktop, Mobile, Tablet } from "./Responsive";

export default function Layout({ Component }) {
  return (
    <div>
      <Desktop>
        <Header></Header>
        <Component></Component>
      </Desktop>
      <Tablet>
        <Header></Header>
        <Component></Component>
      </Tablet>
      <Mobile>
        <Header></Header>
        <Component></Component>
      </Mobile>
    </div>
  );
}
