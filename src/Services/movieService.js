import axios from "axios";
import { BASE_URL, createConfig, config } from "./configURL";

export const movieService = {
  getDanhSachPhim: () => {
    return axios({
      url: `${BASE_URL}QuanLyPhim/LayDanhSachPhim?maNhom=GP03`,
      method: "GET",
      headers: config,
    });
  },
  getDanhSachPhimTheoRap: () => {
    return axios({
      url: `${BASE_URL}QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP03`,
      method: "GET",
      headers: config,
    });
  },
  getDetailMovie: (maPhim) => {
    return axios({
      url: `${BASE_URL}QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${maPhim}`,
      method: "GET",
      headers: config,
    });
  },
  getLichChieu: (maLichChieu) => {
    return axios({
      url: `${BASE_URL}QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${maLichChieu}`,
      method: "GET",
      headers: config,
    });
  },
  datve: (data, access) => {
    return axios({
      url: `${BASE_URL}QuanLyDatVe/DatVe`,
      method: "POST",
      data: data,
      headers: {
        TokenCybersoft:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCBTw6FuZyAwNSIsIkhldEhhblN0cmluZyI6IjI4LzA1LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY4NTIzMjAwMDAwMCIsIm5iZiI6MTY2MjMxMDgwMCwiZXhwIjoxNjg1Mzc5NjAwfQ.FtGbsXl4qyqTRfJrunro0mQ7b-tNs8EWbhb7JDTzloE",
        Authorization: "Bearer " + access,
      },
    });
  },
  getBanner: () => {
    return axios({
      url: `${BASE_URL}QuanLyPhim/LayDanhSachBanner`,
      method: "GET",
      headers: createConfig(),
    });
  },
  getMovieByName: (tenPhim) => {
    return axios({
      url: `${BASE_URL}QuanLyPhim/LayDanhSachPhim?maNhom=GP03&tenPhim=${tenPhim}`,
      method: "GET",
      headers: config,
    });
  },
};
