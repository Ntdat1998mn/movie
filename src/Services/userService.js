import axios from "axios";
import { BASE_URL, createConfig } from "./configURL";

export const userService = {
  postDangNhap: (dataUser) => {
    return axios({
      url: `${BASE_URL}QuanLyNguoiDung/DangNhap`,
      method: "POST",
      data: dataUser,
      headers: createConfig(),
    });
  },
  postDangKy: (dataUser) => {
    return axios({
      url: `${BASE_URL}QuanLyNguoiDung/DangKy`,
      method: "POST",
      data: dataUser,
      headers: createConfig(),
    });
  },
  getDanhSachPhim: () => {
    return axios({
      url: `${BASE_URL}QuanLyPhim/LayDanhSachPhim`,
      method: "GET",
      headers: createConfig(),
    });
  },
};
