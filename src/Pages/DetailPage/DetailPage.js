import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { movieService } from "../../Services/movieService";
import moment from "moment/moment";
import { Rate } from "antd";
import { Progress, Space } from "antd";
import DetailTab from "./DetailTab";
import DetailPageCss from "./DetailPage.css";
import "../../assets/modal-video.scss";
import ModalVideo from "react-modal-video";
import { Desktop, Mobile, Tablet } from "../../HOC/Responsive";

export default function DetailPage() {
  let params = useParams();
  const [isOpen, setOpen] = useState(false);
  const [movie, setMovie] = useState({});
  useEffect(() => {
    movieService
      .getDetailMovie(params.id)
      .then((res) => {
        console.log(res.data.content);
        setMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderDetailMovie = () => {
    return (
      <div className="flex mb-10 bg-slate-50 py-3">
        <div id="movieDetail">
          <img className="object-cover" src={movie.hinhAnh} alt="" />
          <div className="playButton text-center">
            <button onClick={() => setOpen(true)}>
              <img src="../assets/img/play-button.png" alt="" />
            </button>
          </div>
        </div>

        <div className="ml-5 grid grid-cols-2">
          <div>
            <h2 className="text-3xl text-red-500 font-bold mb-2">
              {movie.tenPhim}
            </h2>
            <h4 className="text-xl mb-2">
              {moment(movie.ngayGioChieu).format("DD-MM-YYYY")}
            </h4>
          </div>
          <div className="text-center">
            <Space className="block" wrap>
              <Progress
                type="circle"
                percent={movie.danhGia * 10}
                format={(percent) => `${movie.danhGia}`}
              />
            </Space>
            <Rate disabled allowHalf value={movie.danhGia / 2} />
          </div>
          <p className="col-span-2">{movie.moTa}</p>
        </div>
      </div>
    );
  };
  let renderDetailMovieSmall = () => {
    return (
      <div className="mb-10 bg-slate-50 py-3">
        <div className="grid grid-cols-2">
          <div id="movieDetail">
            <img className="object-cover w-full" src={movie.hinhAnh} alt="" />
            <div className="playButton text-center">
              <button onClick={() => setOpen(true)}>
                <img src="../assets/img/play-button.png" alt="" />
              </button>
            </div>
          </div>
          <div>
            <h2 className=" text-2xl text-red-500 font-bold mb-2">
              {movie.tenPhim}
            </h2>
            <h4 className="text-xl mb-2">
              {moment(movie.ngayGioChieu).format("DD-MM-YYYY")}
            </h4>
            <Rate disabled allowHalf value={movie.danhGia / 2} />
          </div>
        </div>
        <p className="p-2">{movie.moTa}</p>
      </div>
    );
  };
  return (
    <div id="detailPage" className="relative">
      <Desktop>
        <div className="container mx-auto mt-5 w-4/5">
          {renderDetailMovie()}
          <DetailTab movie={movie}></DetailTab>
        </div>
      </Desktop>
      <Tablet>
        <div className="container mx-auto mt-5 w-full">
          {renderDetailMovieSmall()}
          <DetailTab movie={movie}></DetailTab>
        </div>
      </Tablet>
      <Mobile>
        <div className="container mx-auto mt-5 w-full">
          {renderDetailMovieSmall()}
          <DetailTab movie={movie}></DetailTab>
        </div>
      </Mobile>
      <React.Fragment>
        <ModalVideo
          channel="youtube"
          autoplay
          isOpen={isOpen}
          videoId={movie.trailer?.slice(29)}
          onClose={() => setOpen(false)}
        />
      </React.Fragment>
    </div>
  );
}
