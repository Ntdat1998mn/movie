import React from "react";
import { Tabs } from "antd";
import moment from "moment/moment";
import { NavLink } from "react-router-dom";

export default function DetailTab({ movie }) {
  const renderLichChieu = (lichChieuPhim) => {
    return lichChieuPhim.map((lichChieu) => {
      return (
        <NavLink to={`/purchase/${lichChieu.maLichChieu}`}>
          <p className="mr-3 rounded py-1 px-2 bg-slate-300 font-bold text-blue-500">
            {moment(lichChieu.ngayChieuGioChieu).format("DD-MM-YYYY ~ ")}
            <span className="text-red-500">
              {moment(lichChieu.ngayChieuGioChieu).format("hh:mm")}
            </span>
          </p>
        </NavLink>
      );
    });
  };
  const renderCumRap = (heThongRap) => {
    return heThongRap.cumRapChieu?.map((cumRapChieu) => {
      return (
        <>
          <h2 className="text-lg font-bold text-green-500">
            {cumRapChieu.tenCumRap}
          </h2>
          <p className="text-gray-500">{cumRapChieu.diaChi}</p>
          <div className="flex">
            {renderLichChieu(cumRapChieu.lichChieuPhim)}
          </div>
        </>
      );
    });
  };
  const renderTab = () => {
    return movie.heThongRapChieu?.map((heThongRap) => {
      return {
        label: <img style={{ width: "60px" }} src={heThongRap.logo} alt="" />,
        key: heThongRap.maHeThongRap,
        children: renderCumRap(heThongRap),
      };
    });
  };
  return (
    <div className="bg-slate-50">
      <Tabs
        style={{ height: "400px" }}
        className="overflow-scroll"
        defaultActiveKey="1"
        tabPosition="left"
        items={renderTab()}
      />
    </div>
  );
}
