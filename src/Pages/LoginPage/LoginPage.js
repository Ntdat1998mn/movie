import { message } from "antd";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import { setUserInfor } from "../../Redux-toolkit/slice/userSlice";
import { userLocalService } from "../../Services/localStorageService";
import { userService } from "../../Services/userService";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  setFormData,
  setFormErrors,
} from "../../Redux-toolkit/slice/formSlice";

export default function LoginPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const formData = useSelector((state) => state.formSlice.data);
  const formErrors = useSelector((state) => state.formSlice.errors);
  const handleChange = (event) => {
    const { name, value } = event.target;
    let errors = Object.assign({}, formErrors);
    dispatch(setFormData({ [name]: value }));
    //   kiểm tra rỗng + mk < 6
    if (value.trim() === "") {
      errors = { ...errors, [`${name}Err`]: "Trường này không thể để trống" };
    } else if (value.trim().length < 6) {
      errors = {
        ...errors,
        [`matKhauErr`]: "Mật khẩu phải có ít nhất 6 ký tự",
      };
    } else {
      delete errors[`${name}Err`];
    }
    dispatch(setFormErrors(errors));
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    userService
      .postDangNhap(formData)
      .then((res) => {
        dispatch(setUserInfor(res.data.content));
        message.success("Đăng Nhập Thành Công");
        userLocalService.set(res.data.content);
        setTimeout(() => {
          navigate("/");
        }, 1000);
      })
      .catch((err) => {
        message.error("Tài khoản hoặc mật khẩu không đúng!");
        console.log(err);
      });
  };
  return (
    <>
      <div className="flex justify-center w-screen min-h-screen bg-[url('../public/assets/img/backapp.b46ef3a1.jpg')] bg-cover">
        <div className="rounded w-2/5 max-[600px]:w-full my-[120px] bg-white items-center p-4">
          <div className=" flex justify-center">
            <div className="bg-red-500 rounded-full h-10 w-10 leading-10 text-center">
              <FontAwesomeIcon
                className="text-xl text-white"
                icon="fa-solid fa-user"
              />
            </div>
          </div>
          <div>
            <h4 className="font-medium text-xl text-center">Đăng nhập</h4>
          </div>
          <form onSubmit={handleSubmit}>
            <div className="form-group">
              <input
                type="text"
                name="taiKhoan"
                className="form-control mt-3"
                id="staticEmail"
                placeholder="Tài Khoản *"
                onChange={handleChange}
                value={formData.taiKhoan}
              />
              {formErrors.taiKhoanErr && (
                <div className="text-red-500 text-left">
                  {formErrors.taiKhoanErr}
                </div>
              )}
              <input
                type="password"
                className="form-control mt-3"
                name="matKhau"
                id="inputPassword"
                placeholder="Mật khẩu *"
                value={formData.matKhau}
                onChange={handleChange}
              />
              <span className="text-red-500 text-left text-sm">
                {formErrors.matKhauErr}
              </span>
            </div>
            <div className="form-check flex justify-start">
              <label className="form-check-label">
                <input
                  type="checkbox"
                  className="form-check-input"
                  name
                  id
                  defaultValue="checkedValue"
                  defaultChecked
                />
                Nhớ tài khoản
              </label>
            </div>
            <button
              type="submit"
              className="btn bg-red-500 text-white w-full mt-3 active:bg-green-500"
            >
              Đăng Nhập
            </button>
            <NavLink to="/signup">
              <h5 className="mt-3 text-right underline">
                Bạn chưa có tài khoản? Đăng ký
              </h5>
            </NavLink>
          </form>
        </div>
      </div>
    </>
  );
}
