import React from "react";

export default function TabItem3({ item, index }) {
  return (
    <div key={`tab3-${index}`} className="flex mb-6">
      <div className="overflow-hidden rounded w-1/5">
        <img className="rounded" src={item.hinhAnh} alt="" />
      </div>
      <a className="w-4/5" target={"_blank"} href={item.link}>
        <h4 className="pl-2 hover:text-orange-600">{item.tieuDe}</h4>
      </a>
    </div>
  );
}
