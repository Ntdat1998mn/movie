import React from "react";

export default function TabItem2({ item, index }) {
  return (
    <div key={`tab2-${index}`}>
      <div className="rounded overflow-hidden w-full">
        <img src={item.hinhAnh} alt="" />
      </div>
      <a target={"_blank"} href={item.link}>
        <h4 className="pt-3 text-base font-medium hover:text-orange-600">
          {item.tieuDe}
        </h4>
      </a>
      <p className="pt-3 pb-5 text-xs">
        {item.chiTiet.length < 150 + ". . ."
          ? item.chiTiet
          : item.chiTiet.slice(0, 150) + ". . ."}
      </p>
    </div>
  );
}
