import React, { useState } from "react";
import { Card } from "antd";
import TabItem1 from "./TabItem1";
import TabItem2 from "./TabItem2";
import TabItem3 from "./TabItem3";
const { Meta } = Card;

export default function NewsTabs({ data }) {
  /* Nút xem them và rút gọn */
  const [hiddenClassName, setHiddenClasseName] = useState("hidden");
  let xemThem = () => {
    setHiddenClasseName("");
  };
  let rutGon = () => {
    setHiddenClasseName("hidden");
  };
  /* Tạo item */
  let renderItem1 = () => {
    return data.slice(0, 2).map((item, index) => {
      return <TabItem1 item={item} index={index} />;
    });
  };
  let renderItem2 = () => {
    return data.slice(2, 4).map((item, index) => {
      return <TabItem2 item={item} index={index} />;
    });
  };
  let renderItem3 = () => {
    return data.slice(4, 8).map((item, index) => {
      return <TabItem3 item={item} index={index} />;
    });
  };
  return (
    <div>
      <div className={`flex flex-wrap pb-10 ${hiddenClassName}`}>
        <div className="grid grid-cols-1 min-[960px]:grid-cols-2 gap-5">
          {renderItem1()}
        </div>
        <div className="grid grid-cols-1 min-[960px]:grid-cols-3 gap-5">
          {renderItem2()}
          <div>{renderItem3()}</div>
        </div>
      </div>
      <div className="flex justify-center mb-16">
        {hiddenClassName !== "" ? (
          <button
            onClick={xemThem}
            className="border border-slate-400 border-solid rounded px-5 py-2 text-gray-600 hover:bg-orange-600 hover:text-white font-medium"
          >
            XEM THÊM
          </button>
        ) : (
          <button
            onClick={rutGon}
            className="border border-slate-400 border-solid rounded px-5 py-2 text-gray-600 hover:bg-orange-600 hover:text-white font-medium"
          >
            RÚT GỌN
          </button>
        )}
      </div>
    </div>
  );
}
