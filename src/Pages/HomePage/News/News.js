import React from "react";
import { Tabs } from "antd";
import NewsTabs from "./NewsTabs";
import { dataNews } from "../../../assets/data/dataNews";

export default function News() {
  return (
    <div className="news container w-4/5 mx-auto">
      <Tabs
        defaultActiveKey="1"
        centered
        items={[
          {
            label: "Điện Ảnh 24h",
            key: "1",
            children: <NewsTabs data={dataNews.dienAnh} />,
          },
          {
            label: "Review",
            key: "2",
            children: <NewsTabs data={dataNews.review} />,
          },
          {
            label: "Khuyến mãi",
            key: "3",
            children: <NewsTabs data={dataNews.khuyenMai} />,
          },
        ]}
      />
    </div>
  );
}
