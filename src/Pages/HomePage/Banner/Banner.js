import React, { useEffect, useState } from "react";
import { movieService } from "../../../Services/movieService";
import { Carousel } from "antd";
import ModalVideo from "react-modal-video";

const contentStyle = {
  margin: 0,
  height: "580px",
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  background: "#364d79",
};
export default function Banner() {
  const [bannerArr, setBannerArr] = useState([]);
  useEffect(() => {
    movieService
      .getBanner()
      .then((res) => {
        setBannerArr(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const onChange = (currentSlide) => {
    // console.log(currentSlide);
  };
  let [linkYoutube, setLinkYoutube] = useState("");
  const [isOpen, setOpen] = useState(false);
  let openTrailer = (link) => {
    setOpen(true);
    setLinkYoutube(link);
  };
  const renderBanner = () => {
    for (let i = 0; i < bannerArr.length; i++) {
      return (
        <Carousel afterChange={onChange} dotPosition="bottom" autoplay="true">
          <div className="relative">
            <img
              style={contentStyle}
              className="w-full object-cover"
              src={bannerArr[i++].hinhAnh}
              alt=""
            />
            <div className="absolute top-0 left-0 w-full h-full flex items-center justify-center">
              <button onClick={() => openTrailer("uqJ9u7GSaYM")}>
                <img
                  className="w-32 h-32"
                  src="../assets/img/play-button.png"
                  alt=""
                />
              </button>
            </div>
          </div>
          <div className="relative">
            <img
              style={contentStyle}
              className="w-full object-cover"
              src={bannerArr[i++].hinhAnh}
              alt=""
            />
            <div className="absolute top-0 left-0 w-full h-full flex items-center justify-center">
              <button onClick={() => openTrailer("QnrHVynRwTM")}>
                <img
                  className="w-32 h-32"
                  src="../assets/img/play-button.png"
                  alt=""
                />
              </button>
            </div>
          </div>
          <div className="relative">
            <img
              style={contentStyle}
              className="w-full object-cover"
              src={bannerArr[i++].hinhAnh}
              alt=""
            />
            <div className="absolute top-0 left-0 w-full h-full flex items-center justify-center">
              <button onClick={() => openTrailer("_rUC3-pNLyc")}>
                <img
                  className="w-32 h-32"
                  src="../assets/img/play-button.png"
                  alt=""
                />
              </button>
            </div>
          </div>
        </Carousel>
      );
    }
  };

  return (
    <div className="mt-16">
      {renderBanner()}
      <React.Fragment>
        <ModalVideo
          channel="youtube"
          autoplay
          isOpen={isOpen}
          videoId={linkYoutube}
          onClose={() => setOpen(false)}
        />
      </React.Fragment>
    </div>
  );
}
