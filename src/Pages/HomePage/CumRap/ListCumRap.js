import React, { useEffect } from "react";
import { Tabs } from "antd";
import MovieTabItem from "./MovieTabItem";
import MovietabCss from "./MovieTab.css";
import { movieService } from "../../../Services/movieService";
import { useDispatch, useSelector } from "react-redux";
import { setDataMovie } from "../../../Redux-toolkit/slice/movieSlice";

export default function ListCumRap() {
  const dispatch = useDispatch();
  const dataMovie = useSelector((state) => state.movieSlice.dataMovie);
  useEffect(() => {
    movieService
      .getDanhSachPhimTheoRap()
      .then((res) => {
        dispatch(setDataMovie(res.data.content));
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const renderCumRap = () => {
    return dataMovie.map((cumRap) => {
      return {
        label: (
          <img
            className=""
            style={{ width: "60px" }}
            src={cumRap.logo}
            alt=""
          />
        ),
        key: cumRap.maHeThongRap,
        children: (
          <Tabs
            defaultActiveKey="1"
            tabPosition="left"
            items={cumRap.lstCumRap.map((rapPhim) => {
              return {
                label: (
                  <div className="tabList">
                    <h2 className="text-lg text-red-500 font-semibold">
                      {rapPhim.tenCumRap}
                    </h2>
                    <p className="text-neutral-900">
                      {rapPhim.diaChi.length < 60
                        ? rapPhim.diaChi
                        : rapPhim.diaChi.slice(0, 60) + "..."}
                    </p>
                  </div>
                ),
                key: rapPhim.maCumRap,
                children: (
                  <MovieTabItem movieList={rapPhim.danhSachPhim}></MovieTabItem>
                ),
              };
            })}
          />
        ),
      };
    });
  };
  return (
    <div className="bg-slate-50">
      <div className="pb-10 w-4/5 container mx-auto">
        <Tabs
          className="cum__rap"
          defaultActiveKey="1"
          tabPosition="left"
          items={renderCumRap()}
        />
      </div>
    </div>
  );
}
