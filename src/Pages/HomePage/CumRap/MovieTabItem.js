import React from "react";
import moment from "moment/moment";
import { NavLink } from "react-router-dom";

export default function MovieTabItem({ movieList }) {
  const renderMovieList = () => {
    return movieList.map((movie) => {
      return (
        <div className="p-2 flex ">
          <img
            className="object-cover mr-2"
            style={{ width: "180px" }}
            src={movie.hinhAnh}
            alt=""
          />
          <div>
            <h3 className="font-bold text-xl pb-2">{movie.tenPhim}</h3>
            <div className="grid grid-cols-2 gap-3">
              {movie.lstLichChieuTheoPhim.slice(0, 4).map((gioChieu) => {
                return (
                  <NavLink to={`/purchase/${gioChieu.maLichChieu}`}>
                    <p className="rounded bg-red-500 text-white p-2 font-bold">
                      {moment(gioChieu.ngayChieuGioChieu).format(
                        "DD-MM-YYYY ~ "
                      )}
                      <span className="text-yellow-500">
                        {moment(gioChieu.ngayChieuGioChieu).format("hh:mm")}
                      </span>
                    </p>
                  </NavLink>
                );
              })}
            </div>
          </div>
        </div>
      );
    });
  };
  return <div>{renderMovieList()}</div>;
}
