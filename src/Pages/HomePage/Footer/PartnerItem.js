import React from "react";

export default function PartnerItem({ partner, index }) {
  return (
    <a
      key={index}
      className="w-8 h-8 rounded-full truncate mb-5"
      target="_blank"
      href={partner.lienKet}
    >
      <img
        className="hover:contrast-50"
        width={"100%"}
        src={partner.hinhAnh}
        alt={partner.ghiChu}
      />
    </a>
  );
}
