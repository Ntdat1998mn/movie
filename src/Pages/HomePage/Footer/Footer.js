import React from "react";
import { dataPartners } from "../../../assets/data/dataPartner";
import PartnerItem from "./PartnerItem";

export default function Footer() {
  let partners = dataPartners;
  let renderPartnerItem = () => {
    return partners.map((item, index) => {
      return <PartnerItem partner={item} index={index} />;
    });
  };
  return (
    <div className="footer text-white text-xs">
      <div className="container mx-auto flex py-3 w-4/5 max-[960px]:hidden">
        <div className="w-1/3">
          <div>
            <div className="py-3">TIX</div>
          </div>
          <div className="grid grid-cols-2 ">
            <div className="text-gray-400 pb-1 hover:text-white ">FAQ</div>
            <div className="text-gray-400 pb-1 hover:text-white ">
              Brand Guidelines
            </div>
            <div className="text-gray-400 pb-1 hover:text-white ">
              Thỏa thuận sử dụng
            </div>
            <div className="text-gray-400 pb-1 hover:text-white ">
              Chính sách bảo mật
            </div>
          </div>
        </div>
        <div className="w-1/3">
          <div>
            <div className="py-3">ĐỐI TÁC</div>
          </div>
          <div className="grid grid-cols-4">{renderPartnerItem()}</div>
        </div>
        <div className="w-1/3 flex">
          <div className="w-1/2">
            <div className="py-3">MOBILE APP</div>
            <div className="flex">
              <a
                target="_blank"
                href="https://apps.apple.com/vn/app/tix-%C4%91%E1%BA%B7t-v%C3%A9-nhanh-nh%E1%BA%A5t/id615186197"
              >
                <img
                  className="w-8 h-8 rounded-full truncate mr-2"
                  src="./assets/img/footer/mobile_app/apple.png"
                  alt=""
                />
              </a>
              <a
                target="_blank"
                href="https://play.google.com/store/apps/details?id=vn.com.vng.phim123"
              >
                <img
                  className="w-8 h-8 rounded-full truncate"
                  src="./assets/img/footer/mobile_app/android.png"
                  alt=""
                />
              </a>
            </div>
          </div>
          <div className="w-1/2">
            <div className="py-3">SOCIAL</div>
            <div className="flex">
              <a target="_blank" href="https://www.facebook.com/">
                <img
                  className="w-8 h-8 rounded-full truncate mr-2"
                  src="./assets/img/footer/social/facebook.png"
                  alt=""
                />
              </a>

              <a target="_blank" href="https://id.zalo.me/">
                <img
                  className="w-8 h-8 rounded-full truncate"
                  src="./assets/img/footer/social/zalo.png"
                  alt=""
                />
              </a>
            </div>
          </div>
        </div>
      </div>
      <div className="container mx-auto border-t-2 border-gray-400 min-[600px]:flex py-6 w-4/5">
        <div className="min-[600px]:w-1/6 mx-auto max-[600px]:w-40 max-[600px]:flex max-[600px]:justify-center">
          <img className="w-32" src="./assets/img/footer/zion.jpg" alt="" />
        </div>
        <div className="min-[600px]:w-4/6 px-5 max-[600px]:mx-auto max-[600px]:my-5 max-[960px]:text-center">
          <p className="pb-3">TIX – SẢN PHẨM CỦA CÔNG TY CỔ PHẦN ZION</p>
          <p className="pb-1">
            Địa chỉ: Z06 Đường số 13, Phường Tân Thuận Đông, Quận 7, Tp. Hồ Chí
            Minh, Việt Nam.
          </p>
          <p className="pb-1">
            Giấy chứng nhận đăng ký kinh doanh số: 0101659783,
            <br /> đăng ký thay đổi lần thứ 30, ngày 22 tháng 01 năm 2020 do Sở
            kế hoạch và đầu tư Thành phố Hồ Chí Minh cấp.
          </p>
          <p className="pb-1">Số Điện Thoại (Hotline): 1900 545 436</p>
        </div>
        <div className="min-[600px]:w-1/6 mx-auto max-[600px]:w-40 max-[600px]:flex max-[600px]:justify-center">
          <img
            className="w-24"
            src="./assets/img/footer/bocongthuong.png"
            alt=""
          />
        </div>
      </div>
    </div>
  );
}
