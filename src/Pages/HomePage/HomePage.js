import { Desktop, Mobile, Tablet } from "../../HOC/Responsive";
import ListCumRap from "./CumRap/ListCumRap";
import MovieList from "./MovieList/MovieList";
import AppConnect from "./AppConnect/AppConnect";
import Footer from "./Footer/Footer";
import Banner from "./Banner/Banner";
import News from "./News/News";
import FastTicket from "./FastTicket/FastTicket";
import SearchBar from "./SearchBar/SearchBar";
import styleAppConnectNewsFooter from "../../assets/css/style.css";

export default function HomePage() {
  return (
    <>
      <Desktop>
        <Banner></Banner>
        <FastTicket></FastTicket>
        <MovieList></MovieList>
        <ListCumRap></ListCumRap>
        <News></News>
        <AppConnect></AppConnect>
        <Footer></Footer>
      </Desktop>
      <Tablet>
        <Banner></Banner>
        <FastTicket></FastTicket>
        <MovieList></MovieList>
        <News></News>
        <AppConnect></AppConnect>
        <Footer></Footer>
      </Tablet>
      <Mobile>
        <SearchBar></SearchBar>
        <MovieList></MovieList>
        <News></News>
        <AppConnect></AppConnect>
        <Footer></Footer>
      </Mobile>
    </>
  );
}
