import React, { useEffect, useState } from "react";
import { Card } from "antd";
import { Pagination } from "antd";
import MovieListCss from "./MovieList.css";
import { NavLink } from "react-router-dom";
import ModalVideo from "react-modal-video";
import { Desktop, Mobile, Tablet } from "../../../HOC/Responsive";
import { setMovieArr } from "../../../Redux-toolkit/slice/movieSlice";
import { movieService } from "../../../Services/movieService";
import { useDispatch, useSelector } from "react-redux";
import { setMovieList } from "../../../Redux-toolkit/slice/fastTicketRedux";

const { Meta } = Card;

export default function MovieList() {
  let dispatch = useDispatch();
  const [current, setCurrent] = useState(1);
  const [isOpen, setOpen] = useState(false);
  const [link, setLink] = useState("");
  let handleOpenModal = (linkYoututbe) => {
    setOpen(true);
    setLink(linkYoututbe);
  };
  const onChange = (page) => {
    setCurrent(page);
  };
  const movieArr = useSelector((state) => state.movieSlice.movieArr);
  useEffect(() => {
    movieService
      .getDanhSachPhim()
      .then((res) => {
        dispatch(setMovieArr(res.data.content));
        dispatch(setMovieList(res.data.content));
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const renderMovieList = () => {
    let page = (current - 1) * 4;
    return movieArr?.slice(page, page + 4).map((item) => {
      return (
        <div className="flex justify-center">
          <Card
            hoverable
            style={{
              width: 240,
            }}
            cover={
              <img
                className="h-80 object-cover"
                alt="example"
                src={item.hinhAnh}
              />
            }
          >
            <Meta
              title={item.tenPhim}
              description={
                item.moTa.length < 60
                  ? item.moTa
                  : item.moTa.slice(0, 59) + " ..."
              }
            />
            <div className="movieAction">
              <div className="playButton text-center">
                <button
                  onClick={() => handleOpenModal(item.trailer?.slice(29))}
                >
                  <img src="../assets/img/play-button.png" alt="" />
                </button>
              </div>
              <div className="movieButton">
                <NavLink
                  className="hover:text-red-500 text-lg"
                  to={`/detail/${item.maPhim}`}
                >
                  <button className="bg-red-500 text-white rounded py-2 px-4 mr-2 font-bold hover:bg-white hover:text-red-500 ">
                    Xem Chi Tiết
                  </button>
                </NavLink>
              </div>
            </div>
          </Card>
        </div>
      );
    });
  };
  return (
    <div className="w-4/5 container mx-auto">
      <Desktop>
        <div className="movieList grid grid-cols-4 gap-4">
          {renderMovieList()}
        </div>
        <div className="my-5 flex justify-center">
          <Pagination current={current} onChange={onChange} total={40} />
        </div>
        <React.Fragment>
          <ModalVideo
            channel="youtube"
            autoplay
            isOpen={isOpen}
            videoId={link}
            onClose={() => setOpen(false)}
          />
        </React.Fragment>
      </Desktop>
      <Tablet>
        <div className="movieList grid grid-cols-2 gap-4">
          {renderMovieList()}
        </div>
        <div className="my-5 flex justify-center">
          <Pagination current={current} onChange={onChange} total={40} />
        </div>
        <React.Fragment>
          <ModalVideo
            channel="youtube"
            autoplay
            isOpen={isOpen}
            videoId={link}
            onClose={() => setOpen(false)}
          />
        </React.Fragment>
      </Tablet>
      <Mobile>
        <div className="movieList grid grid-cols-1 gap-4">
          {renderMovieList()}
        </div>
        <div className="my-5 flex justify-center">
          <Pagination current={current} onChange={onChange} total={40} />
        </div>
        <React.Fragment>
          <ModalVideo
            channel="youtube"
            autoplay
            isOpen={isOpen}
            videoId={link}
            onClose={() => setOpen(false)}
          />
        </React.Fragment>
      </Mobile>
    </div>
  );
}
