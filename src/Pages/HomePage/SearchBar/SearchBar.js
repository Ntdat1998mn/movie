import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { setMovieArr } from "../../../Redux-toolkit/slice/movieSlice";
import { movieService } from "../../../Services/movieService";

export default function SearchBar() {
  const dispatch = useDispatch();
  const [movieName, setMovieName] = useState("");
  const handleChange = (event) => {
    event.preventDefault();
    const { value } = event.target;
    setMovieName(value);
    console.log("value: ", value);
  };
  const handleSubmit = () => {
    movieService
      .getMovieByName(movieName)
      .then((res) => {
        dispatch(setMovieArr(res.data.content));
      })
      .catch((err) => {
        console.log(err);
      });
  };
  return (
    <div className="flex bg-white mb-3 rounded mt-[80px] p-0 shadow mx-3">
      <input
        onChange={handleChange}
        type="text"
        className="w-full p-2"
        placeholder="Nhập tên phim"
        value={movieName}
      />
      <button
        onClick={handleSubmit}
        className="bg-red-500 text-white btn active:bg-green-500 px-3 py-2"
      >
        Search
      </button>
    </div>
  );
}
