import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import {
  setShowModalTrue,
  setPriceRedux,
  setDataMovie,
} from "../../Redux-toolkit/slice/purchaseRedux";
import { movieService } from "../../Services/movieService";
import ModalConfirm from "./ModalConfirm";
import ModalPurchase from "./ModalPurchase";
import "./Purchase.css";

export default function PurchasePage() {
  let [isLogin, setIsLogin] = useState(false);
  let dispatch = useDispatch();
  let [data, setData] = useState({});
  let [price, setPrice] = useState(0);
  let [ticket, setTicket] = useState([]);
  // let [confirm,setConfirm] = useState(false);
  let confirm = useSelector((state) => state.purchaseRedux.showModalConfirm);
  // kiem tra da dang nhap hay chua
  let user = useSelector((state) => state.userSlice.userInfor);
  //
  let paramsLichChieu = useParams();
  useEffect(() => {
    movieService
      .getLichChieu(paramsLichChieu.id)
      .then((res) => {
        /* console.log(res.data.content); */
        setData(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let showModalConfirm = () => {
    dispatch(setShowModalTrue(true));
    dispatch(setPriceRedux(price));
    dispatch(setDataMovie(data));
  };
  let handleTicket = (ghe) => {
    let giaVe = price;
    let veDaChon = [...ticket];
    let index = veDaChon.findIndex((item) => item === ghe.tenGhe);
    if (index == -1) {
      veDaChon.push(ghe.tenGhe);
      giaVe += ghe.giaVe;
    } else {
      veDaChon.splice(index, 1);
      giaVe -= ghe.giaVe;
    }
    setPrice(giaVe);
    setTicket(veDaChon);
    document
      .getElementById(`buttonTicket${ghe.maGhe}`)
      .classList.toggle("activeColor");
  };
  let renderTicket = () => {
    return data.danhSachGhe?.map((ghe) => {
      if (ghe.daDat == false) {
        if (ghe.loaiGhe === "Thuong") {
          return (
            <button
              id={`buttonTicket${ghe.maGhe}`}
              onClick={() => handleTicket(ghe)}
              key={ghe.maGhe}
              className="w-7 h-7 bg-slate-200 rounded m-1"
            >
              {ghe.tenGhe}
            </button>
          );
        } else {
          return (
            <button
              id={`buttonTicket${ghe.maGhe}`}
              onClick={() => {
                !user ? <ModalPurchase /> : handleTicket(ghe);
              }}
              key={ghe.maGhe}
              className="w-7 h-7 bg-yellow-500 rounded m-1"
            >
              {ghe.tenGhe}
            </button>
          );
        }
      } else {
        return (
          <button
            disabled
            key={ghe.maGhe}
            className="w-7 h-7 bg-gray-500 rounded m-1"
          >
            X
          </button>
        );
      }
    });
  };
  let renderPurchase = () => {
    return (
      <div className="grid grid-cols-3 pt-4">
        <div id="purchasePage" className="col-span-2 overflow-scroll">
          <div className="ticket w-4/5 mx-auto my-5">{renderTicket()}</div>
          <div className="flex justify-center py-5">
            <div className="mx-3 ">
              <p className="bg-gray-500 w-7 h-7 rounded text-center mx-auto">
                X
              </p>
              <h4>Đã Đặt</h4>
            </div>
            <div className="mx-3">
              <p className="bg-slate-200 w-7 h-7 rounded text-center mx-auto"></p>
              <h4>Ghế Thường</h4>
            </div>
            <div className="mx-3">
              <p className="bg-yellow-500 w-7 h-7 rounded text-center mx-auto"></p>
              <h4>Ghế Vip</h4>
            </div>
          </div>
        </div>
        <div id="infoTicket">
          <h2 className="py-5 text-center font-bold text-3xl text-green-500">
            {price.toLocaleString()} VND
          </h2>
          <p>
            Cụm Rạp:{" "}
            <span className="text-lime-500">
              {data.thongTinPhim?.tenCumRap}
            </span>
          </p>
          <p>
            Rạp:{" "}
            <span className="text-lime-500">{data.thongTinPhim?.tenRap}</span>
          </p>
          <p>
            Địa Chỉ:{" "}
            <span className="text-lime-500">
              {data.thongTinPhim?.diaChi.length > 40
                ? data.thongTinPhim?.diaChi.slice(0, 40) + " ..."
                : data.thongTinPhim?.diaChi}
            </span>
          </p>
          <p>
            Giờ Chiếu:{" "}
            <span className="text-lime-500">
              {data.thongTinPhim?.ngayChieu} -{" "}
              <span className="text-red-500">
                {data.thongTinPhim?.gioChieu}
              </span>
            </span>{" "}
          </p>
          <p>
            Tên Phim:{" "}
            <span className="text-lime-500">{data.thongTinPhim?.tenPhim}</span>
          </p>
          <p>
            Chọn:{" "}
            <span className="text-lime-500">
              {ticket.map((item) => {
                return <span>{item}, </span>;
              })}
            </span>{" "}
          </p>

          <button
            onClick={showModalConfirm}
            className="bg-red-500 text-white w-full py-3 rounded text-2xl "
          >
            Mua Vé
          </button>
        </div>
      </div>
    );
  };
  return (
    <div>
      {user && renderPurchase()}
      {!user && <ModalPurchase />}
      {confirm && <ModalConfirm ticket={ticket} />}
    </div>
  );
}
