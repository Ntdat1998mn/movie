import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { NavLink, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  setFormData,
  setFormErrors,
  setIsValid,
} from "../../Redux-toolkit/slice/formRegSlice";
import { message } from "antd";
import { userService } from "../../Services/userService";

export default function RegisterPage() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const formData = useSelector((state) => state.formRegSlice.data);
  const formErrors = useSelector((state) => state.formRegSlice.errors);
  const isFormValid = useSelector((state) => state.formRegSlice.isFormValid);

  const isValid = () => {
    let errors = Object.assign({}, formErrors);
    for (const name in formData) {
      const value = formData[name];
      if (value.trim() === "") {
        errors = { ...errors, [`${name}Err`]: "Trường này không thể để trống" };
      } else if (name === "matKhau" && value.trim().length < 6) {
        errors = {
          ...errors,
          [`matKhauErr`]: "Mật khẩu phải có ít nhất 6 ký tự",
        };
      }
      //   xác nhận mật khẩu
      else if (name === "matKhauXacNhan" && formData.matKhau !== value) {
        errors = {
          ...errors,
          [`matKhauXacNhanErr`]: "Mật khẩu không trùng khớp",
        };
      }
      // ktra họ tên
      else if (name === "hoTen" && /\d/.test(value)) {
        errors = {
          ...errors,
          [`hoTenErr`]: "Họ tên không thể chứa chữ số",
        };
      } else {
        delete errors[`${name}Err`];
      }
      dispatch(setFormErrors(errors));
      if (Object.keys(errors).length === 0) {
        console.log("Object.keys(errors): ", Object.keys(errors));
        dispatch(setIsValid(true));
      } else {
        dispatch(setIsValid(false));
      }
    }
  };
  const [valid, setValid] = useState(false);
  useEffect(() => {
    if (valid) {
      isValid();
    }
  }, [valid, formData]);
  const handleChange = (event) => {
    const { name, value } = event.target;
    dispatch(setFormData({ [name]: value }));
    if (valid) {
      isValid();
    }
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    setValid(true);
    console.log("isFormValid: ", isFormValid);
    if (isFormValid) {
      const { name, value } = event.target;
      let errors = Object.assign({}, formErrors);

      userService
        .postDangKy(formData)
        .then((res) => {
          console.log("res: ", res);
          message.success("Đăng ký thành công, vui lòng đăng nhập lại");
          setTimeout(() => {
            navigate("/login");
          }, 1000);
        })
        .catch((err) => {
          console.log("err: ", err);
          if (err.response.data.content === "Tài khoản đã tồn tại!") {
            errors = { ...errors, taiKhoanErr: "Tài khoản đã tồn tại!" };
          } else if (err.response.data.content === "Email đã tồn tại!") {
            errors = { ...errors, emailErr: "Email đã tồn tại!" };
          }
          dispatch(setFormErrors(errors));
          message.error("Lỗi");
        });
    } else {
      return;
    }
  };
  return (
    <>
      <div className="flex justify-center w-screen min-h-screen bg-[url('../public/assets/img/backapp.b46ef3a1.jpg')] bg-cover">
        <div className="rounded w-2/5 max-[600px]:w-full my-[120px] bg-white items-center p-4">
          <div className="flex justify-center">
            <div className="bg-red-500 rounded-full h-10 w-10 leading-10 text-center">
              <FontAwesomeIcon
                className="text-xl text-white"
                icon="fa-solid fa-lock"
              />
            </div>
          </div>
          <div>
            <h4 className="font-medium text-xl text-center">Đăng ký</h4>
          </div>
          <form onSubmit={handleSubmit}>
            <div className="form-group">
              <input
                type="text"
                name="taiKhoan"
                className="form-control mt-3"
                placeholder="Tài Khoản *"
                value={formData.taiKhoan}
                onChange={handleChange}
              />

              <span className="text-red-500 text-left text-sm">
                {formErrors.taiKhoanErr}
              </span>
              <input
                type="password"
                className="form-control mt-2"
                name="matKhau"
                placeholder="Mật khẩu *"
                value={formData.matKhau}
                onChange={handleChange}
              />
              <span className="text-red-500 text-left text-sm">
                {formErrors.matKhauErr}
              </span>
              <input
                type="password"
                className="form-control mt-2"
                name="matKhauXacNhan"
                placeholder="Nhập lại mật khẩu *"
                value={formData.matKhauXacNhan}
                onChange={handleChange}
              />
              <span className="text-red-500 text-left text-sm">
                {formErrors.matKhauXacNhanErr}
              </span>
              <input
                type="text"
                name="hoTen"
                className="form-control mt-2"
                placeholder="Họ và tên *"
                value={formData.hoTen}
                onChange={handleChange}
              />
              <span className="text-red-500 text-left text-sm">
                {formErrors.hoTenErr}
              </span>

              <input
                type="email"
                name="email"
                className="form-control mt-2"
                placeholder="Email *"
                value={formData.email}
                onChange={handleChange}
              />
              <span className="text-red-500 text-left text-sm">
                {formErrors.emailErr}
              </span>
              <button
                type="submit"
                className="btn bg-red-500 text-white w-full mt-3 active:bg-green-500"
              >
                Đăng Ký
              </button>
            </div>
          </form>
          <NavLink to="/login">
            <h5 className="mt-3 text-right underline">
              Bạn đã có tài khoản? Đăng nhập
            </h5>
          </NavLink>
        </div>
      </div>
    </>
  );
}
