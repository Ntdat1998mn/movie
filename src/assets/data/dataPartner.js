export const dataPartners = [
  {
    hinhAnh: "./assets/img/footer/partner/logo-1.png",
    lienKet: "https://www.cgv.vn/",
    ghiChu: "",
  },
  {
    hinhAnh: "./assets/img/footer/partner/logo-2.png",
    lienKet: "https://www.bhdstar.vn/",
    ghiChu: "",
  },
  {
    hinhAnh: "./assets/img/footer/partner/logo-3.png",
    lienKet: "https://www.galaxycine.vn/",
    ghiChu: "",
  },
  {
    hinhAnh: "./assets/img/footer/partner/logo-4.png",
    lienKet: "http://cinestar.com.vn/",
    ghiChu: "",
  },
  {
    hinhAnh: "./assets/img/footer/partner/logo-5.png",
    lienKet: "https://lottecinemavn.com/LCHS/index.aspx",
    ghiChu: "",
  },
  {
    hinhAnh: "./assets/img/footer/partner/logo-6.png",
    lienKet: "https://www.megagscinemas.vn/",
    ghiChu: "",
  },
  {
    hinhAnh: "./assets/img/footer/partner/logo-7.jpg",
    lienKet: "https://www.betacinemas.vn/home.htm",
    ghiChu: "",
  },
  {
    hinhAnh: "./assets/img/footer/partner/logo-8.png",
    lienKet: "http://ddcinema.vn/",
    ghiChu: "",
  },
  {
    hinhAnh: "./assets/img/footer/partner/logo-9.png",
    lienKet: "https://touchcinema.com/",
    ghiChu: "",
  },
  {
    hinhAnh: "./assets/img/footer/partner/logo-10.jpg",
    lienKet: "https://cinemaxvn.com/",
    ghiChu: "",
  },
  {
    hinhAnh: "./assets/img/footer/partner/logo-11.png",
    lienKet: "https://starlight.vn/",
    ghiChu: "",
  },
  {
    hinhAnh: "./assets/img/footer/partner/logo-12.png",
    lienKet: "https://www.dcine.vn/",
    ghiChu: "",
  },
  {
    hinhAnh: "./assets/img/footer/partner/logo-13.png",
    lienKet: "https://zalopay.vn/",
    ghiChu: "",
  },
  {
    hinhAnh: "./assets/img/footer/partner/logo-14.png",
    lienKet: "https://www.payoo.vn/",
    ghiChu: "",
  },
  {
    hinhAnh: "./assets/img/footer/partner/logo-15.png",
    lienKet: "https://portal.vietcombank.com.vn/Pages/Home.aspx",
    ghiChu: "",
  },
  {
    hinhAnh: "./assets/img/footer/partner/logo-16.png",
    lienKet: "https://www.payoo.vn/",
    ghiChu: "",
  },
  {
    hinhAnh: "./assets/img/footer/partner/logo-17.png",
    lienKet: "https://www.vietinbank.vn/web/home/vn/index.html",
    ghiChu: "",
  },
  {
    hinhAnh: "./assets/img/footer/partner/logo-18.png",
    lienKet: "https://www.indovinabank.com.vn/",
    ghiChu: "",
  },
  {
    hinhAnh: "./assets/img/footer/partner/logo-19.png",
    lienKet: "https://webv3.123go.vn/",
    ghiChu: "",
  },
  {
    hinhAnh: "./assets/img/footer/partner/logo-20.jpg",
    lienKet: "https://laban.vn/",
    ghiChu: "",
  },
];
