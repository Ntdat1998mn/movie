import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./Pages/HomePage/HomePage";
import DetailPage from "./Pages/DetailPage/DetailPage";
import PurchasePage from "./Pages/PurchasePage/PurchasePage";
import Layout from "../src/HOC/Layout";
import LoginPage from "./Pages/LoginPage/LoginPage";
import SignUpPage from "./Pages/SignUpPage/SignUpPage";
import "../src/fontawesome.js";
import NotFoundPage from "./Pages/NotFoundPage/NotFoundPage";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={<Layout Component={HomePage}></Layout>}
          ></Route>
          <Route
            path="/detail/:id"
            element={<Layout Component={DetailPage}></Layout>}
          ></Route>
          <Route
            path="/purchase/:id"
            element={<Layout Component={PurchasePage}></Layout>}
          ></Route>
          <Route
            path="/login"
            element={<Layout Component={LoginPage}></Layout>}
          ></Route>
          <Route
            path="/signup"
            element={<Layout Component={SignUpPage}></Layout>}
          ></Route>
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
