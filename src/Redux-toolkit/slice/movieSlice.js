import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  movieArr: [],
  dataMovie: [],
};

const movieSlice = createSlice({
  name: "movieSlice",
  initialState,
  reducers: {
    setMovieArr: (state, action) => {
      state.movieArr = action.payload;
    },
    setDataMovie: (state, action) => {
      state.dataMovie = action.payload;
    },
  },
});
export const { setMovieArr, setDataMovie } = movieSlice.actions;
export default movieSlice.reducer;
