import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  movieList: [],
};

const formSlice = createSlice({
  name: "ticket",
  initialState,
  reducers: {
    setMovieList: (state, action) => {
      state.movieList = [...action.payload];
    },
  },
});
export const { setMovieList } = formSlice.actions;
export default formSlice.reducer;
