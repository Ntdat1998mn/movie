import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  data: { taiKhoan: "", matKhau: "" },
  errors: {},
};

const formSlice = createSlice({
  name: "form",
  initialState,
  reducers: {
    setFormData: (state, action) => {
      state.data = { ...state.data, ...action.payload };
    },
    setFormErrors: (state, action) => {
      state.errors = { ...action.payload };
    },
  },
});
export const { setFormData, setFormErrors } = formSlice.actions;
export default formSlice.reducer;
