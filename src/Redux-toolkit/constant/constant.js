export const SET_FORM_DATA = "SET_FORM_DATA";
export const SET_FORM_ERRORS = "SET_FORM_ERRORS";
export const SUBMIT_FORM = "SUBMIT_FORM";
